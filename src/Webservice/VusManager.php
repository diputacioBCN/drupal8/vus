<?php

namespace Drupal\vus\Webservice;

use Drupal\Core\Url;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements vus manager.
 */
class VusManager {

  const VALIDACIO_DRUPAL       = 0;
  const VALIDACIO_VUS_I_DRUPAL = 1;
  const VALIDACIO_VUS_O_DRUPAL = 2;
  const VALIDACIO_VUS          = 3;

  /**
   * Get user data using VUS Webservice.
   */
  public static function getWsData(string $vus_user, string $vus_pass, string $vus_app = '') {
    $config = \Drupal::config('vus.settings');
    $url = $config->get('vus_ws_serv');

    $credentials = [
      'ws_usuari'  => $config->get('vus_ws_usr'),
      'ws_clau'    => $config->get('vus_ws_clau'),
      'usuari_vus' => 'OPS$' . strtoupper($vus_user),
      'clau_vus'   => $vus_pass,
    ];

    if (!empty($vus_app)) {
      $credentials['aplicacio'] = $vus_app;
    }

    $data = \Drupal::httpClient()->post($url, [
      'form_params' => $credentials,
      'headers' => [
        'Content-type' => 'application/x-www-form-urlencoded',
      ],
    ])->getBody()->getContents();

    // Ens assegurem que torna un codi i és el correcte.
    $xml = @simplexml_load_string($data, 'SimpleXMLElement', LIBXML_NOCDATA);
    $reposta = json_decode(json_encode($xml), TRUE);

    $codi = isset($reposta['resposta']['codi_resposta']) ? (int) $reposta['resposta']['codi_resposta'] : 1;
    $codi_motiu = isset($reposta['resposta']['codi_motiu']) ? (int) $reposta['resposta']['codi_motiu'] : 307;

    $access = FALSE;
    if (0 === $codi) {
      // Usuari amb accés vàlid.
      $access = TRUE;
    }
    elseif (1 === $codi && empty($vus_app) && in_array($codi_motiu, [303, 304, 305])) {
      // Usuari amb validació correcte però no te assignada cap aplicació o falten dades.
      // 303: Ens inexistent / 304: Aplicacio inexistent / 305: No te perfil d'aplicació definit.
      $access = TRUE;
    }

    $user = $user_ens = $user_perfils = [];
    if ($access) {
      $user = [
        'username' => !empty($reposta['resposta']['usuari_vus']['usuari']) ? strtolower(str_replace('OPS$', '', $reposta['resposta']['usuari_vus']['usuari'])) : $vus_user,
        'name'     => !empty($reposta['resposta']['usuari_vus']['nom']) ? $reposta['resposta']['usuari_vus']['nom'] : $vus_user,
        'email'    => !empty($reposta['resposta']['usuari_vus']['e_mail']) ? $reposta['resposta']['usuari_vus']['e_mail'] : '',
        'org_codi' => !empty($reposta['resposta']['usuari_vus']['organic']['codi']) ? $reposta['resposta']['usuari_vus']['organic']['codi'] : '',
        'org_nom'  => !empty($reposta['resposta']['usuari_vus']['organic']['descr']) ? $reposta['resposta']['usuari_vus']['organic']['descr'] : '',
      ];

      if (isset($reposta['resposta']['usuari_vus']['lens']['ens'])) {
        // Llista d'ens.
        $lens = $reposta['resposta']['usuari_vus']['lens']['ens'];
        $lens = isset($lens['codi']) ? [$lens] : $lens;

        foreach ($lens as $ens) {
          if (isset($ens['codi'])) {
            $user_ens[$ens['codi']] = $ens['descr'];
            if (isset($ens['laplicacions']['aplicacio'])) {
              // Llista d'aplicacions a dins d'un ens.
              $aplicacions = $ens['laplicacions']['aplicacio'];
              $aplicacions = isset($aplicacions['perfil']) ? [$aplicacions] : $aplicacions;
              foreach ($aplicacions as $aplicacio) {
                if (isset($aplicacio['perfil']) && isset($aplicacio['codi'])) {
                  $nom_perfil = $aplicacio['codi'] . ':' . $aplicacio['perfil']['descr'];
                  if (strlen($nom_perfil) > 255) {
                    $nom_perfil = substr($nom_perfil, 0, 254) . '…';
                  }
                  $user_perfils[$aplicacio['perfil']['codi']] = $nom_perfil;
                }
              }
            }
          }
        }
      }
    }

    // Recollim el missatge de resposta.
    return [
      'access'   => $access,
      'codi'     => $codi,
      'user'     => $user,
      'ens'      => $user_ens,
      'perfils'  => $user_perfils,
      'resposta' => !empty($reposta['resposta']['text_resposta']) ? $reposta['resposta']['text_resposta'] : "Error desconegut de l'accés restringit. Torna-ho a provar en uns minuts.",
    ];
  }

  /**
   * Check if is a diba mail or not.
   */
  public static function isDibaMail(string $mail) {
    if (!empty($mail) && strpos($mail, '@') !== FALSE) {
      $mail_segments = explode('@', $mail);
      $config = \Drupal::config('vus.settings');
      $diba_domains = explode(',', $config->get('vus_diba_domains'));

      if (!empty($mail_segments[1]) && in_array($mail_segments[1], $diba_domains)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Get tid Perfils. Create if not exists.
   */
  private static function getTidsPerfils(array $perfils) {
    $tids = [];
    foreach ($perfils as $codi => $nom) {
      $tids[] = self::getOrCreateTerm('field_vus_perfils_id', $codi, $nom, 'vus_perfils');
    }

    return $tids;
  }

  /**
   * Get tid Ens. Create if not exists.
   */
  private static function getTidsEns(array $ens) {
    $tids = [];
    foreach ($ens as $codi => $nom) {
      $tids[] = self::getOrCreateTerm('field_vus_ens_id', $codi, $nom, 'vus_ens');
    }

    return $tids;
  }

  /**
   * Helper: Tornem o creem termes de Drupal.
   */
  private static function getOrCreateTerm(string $field, string $value, string $name, string $vid) {
    $tid = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', $vid)
      ->condition($field, $value)
      ->execute();

    if (!empty($tid)) {
      // Existeix el terme, tornem el primer.
      $tid = reset($tid);
    }
    else {
      // No existeix el terme, el creem.
      $term = Term::create([
        'name' => $name,
        'vid' => $vid,
        $field => $value,
      ]);
      $term->enforceIsNew();
      $term->save();

      $tid = $term->id();
      \Drupal::logger('vus')->info("El terme @value : @name s'ha creat amb el login del VUS.", [
        '@name' => $name,
        '@value' => $value,
      ]);
    }

    return $tid;
  }

  /**
   * Actualitza un usuari Drupal amb les dades del Webservice.
   */
  public static function updateUser(array $ws, User $user) {
    $config = \Drupal::config('vus.settings');
    // Asssignem el nom si cal.
    $name_field = $config->get('vus_sincro_nom');
    if (!empty($name_field)) {
      $user->set($name_field, $ws['user']['name']);
    }
    // Asssignem els perfils si cal.
    if (1 == $config->get('vus_sincro_perfil')) {
      $tids = self::getTidsPerfils($ws['perfils']);
      $user->set('user_vus_perfils', $tids);
    }
    // Asssignem els ens si cal.
    if (1 == $config->get('vus_sincro_ens')) {
      $tids = self::getTidsEns($ws['ens']);
      $user->set('user_vus_ens', $tids);
    }
    $user->save();

    return $user;
  }

  /**
   * Crea un usuari Drupal amb les dades del Webservice.
   */
  public static function createUser(array $ws) {
    $config = \Drupal::config('vus.settings');

    // Evitem crear usuaris sense correu quan és obligatori.
    if (empty($ws['user']['email']) && 1 == $config->get('vus_email_mandatory')) {
      $msg = new FormattableMarkup('Usuari @username no te un correu assignat al VUS i aquest és obligatori.', [
        '@username' => $ws['user']['username'],
      ]);
      \Drupal::messenger()->addMessage($msg, 'error');

      return FALSE;
    }

    // Evitem crear usuaris amb correus duplicats.
    if (!empty($ws['user']['email']) && FALSE !== user_load_by_mail($ws['user']['email'])) {
      $msg = new FormattableMarkup('No es poden crear dos usuaris amb el mateix correu: @email', [
        '@email' => $ws['user']['email'],
      ]);
      \Drupal::messenger()->addMessage($msg, 'error');

      return FALSE;
    }

    $user = User::create([
      'name' => $ws['user']['username'],
      'mail' => $ws['user']['email'],
      'pass' => hash('crc32b', time()) . rand(100,999),
    ]);
    $role = $config->get('vus_autouser_role');
    if (!empty($role) && !in_array($role, ['anonymous', 'authenticated'])) {
      $user->addRole($role);
    }
    $user->enforceIsNew();
    $user->activate();
    $user->save();

    \Drupal::logger('vus')->info('Usuari @username creat amb el login del VUS.', [
      '@username' => $ws['user']['username'],
    ]);

    // Un cop create l'usuari bàsic actualitzem amb les dades del VUS.
    $user = self::updateUser($ws, $user);

    return $user;
  }

  /**
   * Valida l'usuari, i si està activada la ceació d'usuaris el crea.
   */
  public static function validateUser(string $vus_user, string $vus_pass, string $vus_app = '') {
    $ws = self::getWsData($vus_user, $vus_pass, $vus_app);
    if (TRUE === $ws['access']) {
      $user = user_load_by_name($vus_user);

      if (!empty($user)) {
        // Si hi ha sincronització contínua actualitzem les dades del VUS.
        if (1 == \Drupal::config('vus.settings')->get('vus_sincro_continuous')) {
          $user = self::updateUser($ws, $user);
        }
      }
      else {
        // La validació VUS es correcta però l'usuari no existeix, creem-lo.
        $user = self::createUser($ws);
      }

      return $user;
    }
    else {
      \Drupal::messenger()->addMessage($ws['resposta'], 'error');
    }

    return FALSE;
  }

  /**
   * Login de l'usuari i redirecció cap a la portada.
   */
  public static function login(User $user) {
    if ($user->id() > 0) {
      user_login_finalize($user);

      $settings = \Drupal::config('vus.settings');

      $vus_redirect = $settings->get('vus_redirect');
      if (!empty($vus_redirect)) {
        if ('<front>' === $vus_redirect) {
          $url = Url::fromRoute('<front>');
        }
        elseif ('<user>' === $vus_redirect) {
          $url = Url::fromRoute('entity.user.canonical', ['user' => $user->id()]);
        }
        else {
          $url = URL::fromUserInput($vus_redirect);
        }
        $response = new RedirectResponse($url->toString());

        $middleware = \Drupal::service('vus.http_middleware');
        $middleware->setRedirectResponse($response);
      }
    }
  }

}
