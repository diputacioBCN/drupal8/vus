<?php

namespace Drupal\vus\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\user\Entity\Role;
use Drupal\vus\Webservice\VusManager;

/**
 * Configure Vus settings for this site.
 */
class VusSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vus_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vus.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vus.settings');
    $form['vus_user1_scape'] = [
      '#type' => 'textfield',
      '#title' => 'Ignora la valdiació VUS per aquests usernames',
      '#description' => "Llista d'usernames separats per comes.",
      '#default_value' => $config->get('vus_user1_scape'),
    ];
    $form['vus_validation'] = [
      '#type' => 'select',
      '#title' => 'Tipus de validació',
      '#description' => "Escull un dels tipus de validació d'usuaris. La validació combinada accepta els dos passwords de l'usuari. La validació excloent nómes accepta la contrasenya del VUS quan l'usuari existeix al VUS.",
      '#options' => [
        VusManager::VALIDACIO_DRUPAL       => 'Validació nativa de Drupal',
        VusManager::VALIDACIO_VUS_I_DRUPAL => 'Validació combinada del VUS i del Drupal',
        VusManager::VALIDACIO_VUS_O_DRUPAL => 'Validació excloent del VUS o del Drupal',
        VusManager::VALIDACIO_VUS          => 'Validació només a través del servei web del VUS',
      ],
      '#default_value' => $config->get('vus_validation'),
    ];
    $form['vus_redirect'] = [
      '#type' => 'textfield',
      '#title' => 'Redirecció després de fer autologin',
      '#description' => 'Utilitza una ruta relativa començant per /; Rutes especial: Portada = &lt;front&gt;; Usuari = &lt;user&gt;; camp buit = sense redirecció',
      '#default_value' => $config->get('vus_redirect') ?? '<front>',
    ];
    $form['vus_post'] = [
      '#type' => 'checkbox',
      '#title' => 'Permet fer login passant els paràmetres del VUS per POST',
      '#default_value' => $config->get('vus_post'),
    ];
    $form['vus_email_mandatory'] = [
      '#type' => 'checkbox',
      '#title' => 'Fes el correu obligatori per crear nous usuaris',
      '#default_value' => $config->get('vus_email_mandatory'),
    ];
    $form['vus_autouser'] = [
      '#type' => 'checkbox',
      '#title' => 'Crea automàticament els usuaris que entrin pel VUS si no existeixen',
      '#default_value' => $config->get('vus_autouser'),
    ];
    $form['vus_autouser_role'] = [
      '#type' => 'select',
      '#title' => 'Rol per defecte dels usuaris VUS.',
      '#description' => 'Tria el rol per defecte dels usuaris que es crein automàticamnet.',
      '#options' =>  array_map(function (Role $role) {
          return $role->label();
      }, Role::loadMultiple()),
      '#default_value' => $config->get('vus_autouser_role'),
    ];

    $user_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    $options = [];
    foreach ($user_fields as $key => $field) {
      $options[$key] = $field->getLabel() . ' (' . $key . ')';
    }
    $form['vus_sincro_nom'] = [
      '#type' => 'select',
      '#title' => 'Sincronitza el Nom del VUS',
      '#description' => "Tria el camp de l'usuari on vols que es sincronitzi el Nom.",
      '#options' => $options,
      '#empty_value' => '',
      '#empty_option' => t('- Cap -'),
      '#default_value' => $config->get('vus_sincro_nom'),
    ];
    $form['vus_sincro_ens'] = [
      '#type' => 'checkbox',
      '#title' => "Sincronitza els Ens del VUS",
      '#description' => "Si marques aquesta opció es crearà un vocabulari: 'Ens VUS' i s'asignaran als usuaris els seus ens.",
      '#default_value' => $config->get('vus_sincro_ens'),
    ];
    $form['vus_sincro_perfil'] = [
      '#type' => 'checkbox',
      '#title' => "Sincronitza els Perfils del VUS",
      '#description' => "Si marques aquesta opció es crearà un vocabulari: 'Perfils VUS' i s'asignaran als usuaris els seus perfils.",
      '#default_value' => $config->get('vus_sincro_perfil'),
    ];
    $form['vus_sincro_continuous'] = [
      '#type' => 'checkbox',
      '#title' => "Activa la sincronització contínua de les dades adicionals del VUS",
      '#description' => "Si marques aquesta opció s'actualitzarà el Nom, Ens i Perfil de l'usuari cada cop que l'usuari es loga. Si deixes l'opció desmarcada només es sincronitza l'usuari i la contrasenya.",
      '#default_value' => $config->get('vus_sincro_continuous'),
    ];
    $form['vus_ws_serv'] = [
      '#type' => 'textfield',
      '#title' => 'Ruta del servei web',
      '#default_value' => $config->get('vus_ws_serv'),
    ];
    $form['vus_ws_usr'] = [
      '#type' => 'textfield',
      '#title' => 'Usuari del servei web',
      '#default_value' => $config->get('vus_ws_usr'),
    ];
    $form['vus_ws_clau'] = [
      '#type' => 'textfield',
      '#title' => 'Contrasenya del servei web',
      '#default_value' => $config->get('vus_ws_clau'),
    ];
    $form['vus_ws_app'] = [
      '#type' => 'textfield',
      '#title' => "Codi d'aplicació",
      '#description' => "Codi d'aplicació per limitar l'accés als usuaris d'una certa aplicació (opcional però molt recomanable si actives la sincronització d'ens o perfils, sinó se't plenaran les dades de l'usuari amb tots els ens i perfils que tingui assignat l'usuari de totes les seves aplicacions corporatives).",
      '#default_value' => $config->get('vus_ws_app'),
    ];
    $form['vus_diba_domains'] = [
      '#type' => 'textarea',
      '#title' => "Dominis DiBa",
      '#description' => "Llista de dominis corporatius separats per comes o per salts de línea. Exemple: diba.cat,diba.es. Els correus amb aquests dominis faran la validació només pel VUS si la validació és del tipus Drupal O VUS.",
      '#default_value' => str_replace(',', "\n", $config->get('vus_diba_domains')),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('vus.settings')
      ->set('vus_user1_scape', $form_state->getValue('vus_user1_scape'))
      ->set('vus_validation', $form_state->getValue('vus_validation'))
      ->set('vus_post', $form_state->getValue('vus_post'))
      ->set('vus_redirect', $form_state->getValue('vus_redirect'))
      ->set('vus_email_mandatory', $form_state->getValue('vus_email_mandatory'))
      ->set('vus_autouser', $form_state->getValue('vus_autouser'))
      ->set('vus_autouser_role', $form_state->getValue('vus_autouser_role'))
      ->set('vus_sincro_nom', $form_state->getValue('vus_sincro_nom'))
      ->set('vus_sincro_ens', $form_state->getValue('vus_sincro_ens'))
      ->set('vus_sincro_perfil', $form_state->getValue('vus_sincro_perfil'))
      ->set('vus_sincro_continuous', $form_state->getValue('vus_sincro_continuous'))
      ->set('vus_ws_serv', $form_state->getValue('vus_ws_serv'))
      ->set('vus_ws_usr', $form_state->getValue('vus_ws_usr'))
      ->set('vus_ws_clau', $form_state->getValue('vus_ws_clau'))
      ->set('vus_ws_app', $form_state->getValue('vus_ws_app'))
      ->set('vus_diba_domains', str_replace([' ', "\n", "\t", "\r"], ['', ',', '', ''], $form_state->getValue('vus_diba_domains')))
      ->save();

    if (1 == $form_state->getValue('vus_sincro_ens')) {
      $vocabulary = Vocabulary::load('vus_ens');
      if (empty($vocabulary)) {
        $this->createVocabularyUserMap([
          'vid'              => 'vus_ens',
          'vocabulary_label' => 'Ens del VUS',
          'term_label'       => "ID de l'ens del VUS",
          'user_label'       => "Ens",
        ]);
      }
    }
    if (1 == $form_state->getValue('vus_sincro_perfil')) {
      $vocabulary = Vocabulary::load('vus_perfils');
      if (empty($vocabulary)) {
        $this->createVocabularyUserMap([
          'vid'              => 'vus_perfils',
          'vocabulary_label' => 'Perfils del VUS',
          'term_label'       => "ID del perfil del VUS",
          'user_label'       => "Perfils",
        ]);
      }
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  private function createVocabularyUserMap(array $options) {
    // Creem el nou vocabulary.
    Vocabulary::create([
      'vid'    => $options['vid'],
      'name'   => $options['vocabulary_label'],
      'weight' => 10,
    ])->save();
    // Creem el camp ID pels termes del vocabulary.
    FieldStorageConfig::create([
      'field_name'  => 'field_' . $options['vid'] . '_id',
      'entity_type' => 'taxonomy_term',
      'type'        => 'string',
    ])->save();
    // Vinculem el nou camp als termes del vocabulari.
    FieldConfig::create([
      'field_name'  => 'field_' . $options['vid'] . '_id',
      'entity_type' => 'taxonomy_term',
      'label'       => $options['term_label'],
      'bundle'      => $options['vid'],
      'required'    => 1,
    ])->save();
    // Creem el camp de l'usuari.
    FieldStorageConfig::create([
      'field_name'  => 'user_' . $options['vid'],
      'entity_type' => 'user',
      'type'        => 'entity_reference',
      'cardinality' => -1,
      'settings'    => [
        'target_type' => 'taxonomy_term',
      ],
    ])->save();
    // Vinculem el nou camp al compte d'usuari.
    FieldConfig::create([
      'field_name'  => 'user_' . $options['vid'],
      'entity_type' => 'user',
      'label'       => $options['user_label'],
      'bundle'      => 'user',
      'cardinality' => -1,
      'settings'    => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [$options['vid']],
        ],
      ],
    ])->save();
  }

}
