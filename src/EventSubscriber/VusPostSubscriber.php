<?php

namespace Drupal\vus\EventSubscriber;

use Drupal\Component\Utility\Html;
use Drupal\vus\Webservice\VusManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Subscribe to kernal request event to check authentication.
 */
class VusPostSubscriber implements EventSubscriberInterface {

  /**
   * Validació de l'usuari a través dels paràmetres POST del VUS.
   */
  public function checkForLoginSubmit(RequestEvent $event) {
    // Only login if user is not logged.
    if (\Drupal::currentUser()->isAnonymous()) {
      // Login automàtic si permetem la validació amb paràmetres POST.
      $config = \Drupal::config('vus.settings');
      if (1 == $config->get('vus_post') && $config->get('vus_validation') > 0) {
        // Ens asegurem que rebem les dues variables del vus.
        $vus_user = \Drupal::request()->request->get('user');
        $vus_pass = \Drupal::request()->request->get('pass');
        if (!empty($vus_user) && !empty($vus_pass)) {
          // Recuperem l'usuari. Eliminem el OPS$ de l'usuari si existeix.
          $vus_user = Html::escape(str_replace('ops$', '', strtolower(trim($vus_user))));
          $vus_pass = Html::escape(trim($vus_pass));
          $vus_app = $config->get('vus_ws_app');

          $user = VusManager::validateUser($vus_user, $vus_pass, $vus_app);
          if (!empty($user)) {
            VusManager::login($user);
          }
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['checkForLoginSubmit'];
    return $events;
  }

}
