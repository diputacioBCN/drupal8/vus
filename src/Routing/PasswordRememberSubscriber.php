<?php

namespace Drupal\vus\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;
use Drupal\vus\Webservice\VusManager;

/**
 * Listens to the dynamic route events.
 */
class PasswordRememberSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection) {
    $config = \Drupal::config('vus.settings');
    if (VusManager::VALIDACIO_VUS === $config->get('vus_validation')) {
      if ($route = $collection->get('user.pass')) {
        $route->setRequirement('_access', 'FALSE');
      }
    }
  }

}
